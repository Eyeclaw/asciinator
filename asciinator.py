#!env python
"""
ASCIInator

Author:     David Kubek
Univeristy: MFF UK
Semester:   winter 2019/20
Group:      33

Programovani I NPRG030
"""

import os
import pickle
import re
import string
import sys

from src import GlyphMap, ImagePixels, asciify, filters

USAGE = """Usage: ./asciinator.py image_file

Supported image formats: bmp, jpg, png
NOTE: For transparent images, however, Alpha channel is ignored.

Output:
    -w, --width
        Sets with of the output string

Preprocess options:
    --grayscale
        Set mode for grayscale converstion. Supported modes: luma, mean
        (default is luma)
    --inverted
        Inverts the colors of output
    -k
        Limits the number of colors in image (lower means fewer colors).
        For high values output starts being cluttered. Lower values are
        recommended
    --no-grayscale
        Use if you don't want to perfor explicit grayscale conversion
    --no-normalize
        Don't normalize image
    --no-preprocess
        Dont't do any preprocessing.
    --no-posterize
        Use if you don't want to apply posterize effect

Characters:
    -c, --characters
        Use this to select specific characters in output.
        WARNING: this overrides the dafault characters

    --blacklist
        Use this to blacklist specific characters

Fonts:
    --font
        Use this to specify path to font file you want to use
        NOTE: As of right now only ttf/otf fonts are supported

Others:
    --help
        Prints this message
"""

blocks = "▃▄▅▆▇█▉▊▋▌▍▎▐░▒▓"
default_characters = string.printable[:-5] + blocks

default_options = {
    "width": 80,
    "gray": True,
    "gray_mode": "luma",
    "char_ratio": (1, 2),
    "preprocess": True,
    "inverted": False,
    "font": None,
    "posterize": True,
    "normalize": True,
    "k-value": 3,
    "chars": default_characters,
    "input_file": None,
}

options = {
    "--width": "width",
    "-w": "width",
    "--height": "height",
    "-h": "height",
    "--grayscale": "gray_mode",
    "--inverted": "inverted",
    "-k": "k-value",
    "--no-grayscale": "nogray",
    "--no-posterize": "noposterize",
    "--no-normalize": "nonormalize",
    "--no-preprocess": "nopreprocess",
    "-c": "chars",
    "--characters": "chars",
    "--blacklist": "blacklist",
    "--font": "font",
    "--help": "help",
    "-?": "help",
}

grayscale_options = {"mean", "luma"}


def is_valid_arg(args, p):
    """
    Chceck wether value for option is valid e.g. there is a value and it
    isn't another option.
    """
    return p < len(args) and not re.match("-+", args[p])


def process_opt(opts, args, p):  # NOQA
    """
    Process command line argument at p

    Returns args used.
    """
    key = options.get(args[p])

    if key is None:
        print("INVALID OPTION:", args[p])
        print(USAGE)
        sys.exit(1)

    if key == "help":
        print(USAGE)
        sys.exit(1)

    if key == "width":  # or key == "height"
        if not is_valid_arg(args, p + 1):
            print("No value given for", args[p])
            print(USAGE)
            sys.exit(1)

        if not re.match(r"[0-9]+", args[p + 1]):
            print("Invalid value for {}: {}".format(args[p], args[p + 1]))
            print(USAGE)
            sys.exit(1)

        opts[key] = int(args[p + 1])
        return 2

    if key == "nogray":
        opts["gray"] = False
        return 1

    if key == "gray_mode":
        if not is_valid_arg(args, p + 1):
            print("No value given for", args[p])
            print(USAGE)
            sys.exit(1)

        if args[p + 1] not in grayscale_options:
            print("Grayscale option not supported:", args[p + 1])
            print(USAGE)
            sys.exit(1)

        opts[key] = args[p + 1]
        return 2

    if key == "inverted":
        opts[key] = True
        return 1

    if key == "noposterize":
        opts["posterize"] = False
        return 1

    if key == "nonormalize":
        opts["normalize"] = False
        return 1

    if key == "nopreprocess":
        opts["preprocess"] = False
        return 1

    if key == "k-value":
        if not is_valid_arg(args, p + 1):
            print("No k-value given")
            print(USAGE)
            sys.exit(1)

        if not re.match(r"[0-9]+", args[p + 1]):
            print("Invalid value for {}: {}".format(args[p], args[p + 1]))
            print(USAGE)
            sys.exit(1)

        opts[key] = int(args[p + 1])
        return 2

    if key == "chars":
        if not is_valid_arg(args, p + 1):
            print("No characters given")
            print(USAGE)
            sys.exit(1)

        opts[key] = args[p + 1] + " "
        return 2

    if key == "font":
        if not is_valid_arg(args, p + 1):
            print("No value given for", args[p])
            print(USAGE)
            sys.exit(1)

        if not os.path.isfile(args[p + 1]):
            print("File", args[p + 1], "deasn't exist.")
            sys.exit(1)

        opts[key] = args[p + 1]
        return 2

    if key == "blacklist":
        if not is_valid_arg(args, p + 1):
            print("No characters given")
            print(USAGE)
            sys.exit(1)

        blist = set(args[p + 1])
        old = set(opts["chars"])
        opts["chars"] = str(old.difference(blist))


def preprocess(im, opts):
    if opts["posterize"]:
        im = filters.posterize(im, k=opts["k-value"])

    if opts["gray"]:
        im = filters.grayscale(im, mode=opts["gray_mode"])

    if opts["normalize"]:
        im = filters.normalize(im)

    if opts["inverted"]:
        im = filters.invert(im)

    return im


def main():
    args = sys.argv[1:]

    # Process command line arguments
    opts = default_options
    if len(args) == 0:
        print(USAGE)
        sys.exit(1)

    p = 0
    while p < len(args):
        if re.match("-+", args[p]):
            p += process_opt(opts, args, p)
        elif os.path.isfile(args[p]):
            opts["input_file"] = args[p]
            p += 1
        else:
            print("INVALID OPTION:", args[p])
            print(USAGE)
            sys.exit(1)

    if not opts["input_file"]:
        print("No input file given!")
        print(USAGE)
        sys.exit(1)

    im = ImagePixels.from_file(opts["input_file"])

    if opts["preprocess"]:
        im = preprocess(im, opts)

    if opts["font"]:
        gm = GlyphMap.from_fontfile(opts["font"], chars=opts["chars"])
    else:
        if os.path.isfile("data/default_charmap.pickle"):
            with open("data/default_charmap.pickle", "rb") as fin:
                gm = GlyphMap.from_charmap(pickle.load(fin), opts["chars"])
        else:
            print(
                "Could not find default glyphmap file. Please use --font-ttf\
                   to supply a font file."
            )
            sys.exit(1)

    ascii = asciify.asciify(im, gm, opts["width"], chratio=opts["char_ratio"])
    print(ascii)


if __name__ == "__main__":
    main()
