import bisect
from collections import namedtuple

import numpy as np
from PIL import Image, ImageDraw, ImageFont

# Object containing info about GlyphMap
GMInfo = namedtuple("GMInfo", ("lightest", "darkest"))


class GlyphMap:
    """
    Map from brightness to closest glyph.
    """

    def __init__(self, pairs):
        pairs.sort()
        self.vals = np.array([v for v, _ in pairs], dtype=np.uint8)
        self.chars = [ch for _, ch in pairs]

        self.info = GMInfo(min(self.vals), max(self.vals))

    def __getitem__(self, key):
        index = min(bisect.bisect_left(self.vals, key), len(self.vals) - 1)
        return self.chars[index]


def generate_charmap(font_file, chars):
    """
    Generate map from brightness values to characters from font file

    font_file:  path to font file
    chars:      characters for witch to generate values

    Returns dictionary mapping values from [0, 255] to characters.

    """

    font = ImageFont.truetype(font_file, 72)
    im = Image.new("1", (72, 72))
    draw = ImageDraw.Draw(im)

    charmap = dict()
    for char in chars:
        w, h = draw.textsize(char, font=font)
        im = Image.new("1", (w, h))
        draw = ImageDraw.Draw(im)
        draw.rectangle([0, 0, w, h], fill="#FFFFFF")
        draw.text((0, 0), char, font=font, fill="#000000")

        charmap[char] = np.uint8((1 - np.mean(np.array(im))) * 255)

    return charmap


def from_fontfile(font_file, chars):
    """
    Generate GlyphMap from font file

    font_file:  path to font file
    chars:      characters for witch to generate values

    Returns new GlyphMap object

    """

    # TODO: Check if truetype or bitmap
    return from_charmap(generate_charmap(font_file, chars), chars)


def from_charmap(charmap, chars):
    """
    Generate GlyphMap from given character map.

    charmap: character map
    chars:      characters for witch to generate values

    Returns new GlyphMap object.

    """

    pairs = []
    for char in chars:
        pairs.append((charmap[char], char))

    return GlyphMap(pairs)
