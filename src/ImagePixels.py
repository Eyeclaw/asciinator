from collections import namedtuple

import numpy as np
from PIL import Image

IPInfo = namedtuple("IPInfo", ("width", "height"))


def from_file(filename):
    im = Image.open(filename)
    im = im.convert(mode="RGB")

    info = IPInfo(im.width, im.height)
    data = np.array(im)[..., :3]
    return ImagePixels(data, info)


class ImagePixels:
    """ Represents pixels of an image.  """

    def __init__(self, data, info):
        self.data = data
        self.info = info

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, val):
        self.data[key] = val

    def save_img(self, out_file):
        """
        Save image to output file given by file name. Output format
        is determined by extension

        out_file: string
        """
        im_out = Image.fromarray(self.data)
        im_out.save(out_file)
